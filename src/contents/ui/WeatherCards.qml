import QtQuick 2.6
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.5 as Controls
Kirigami.ScrollablePage {

    Kirigami.CardsListView{
        id: view
        model: ListModel {
            id: mainModel
        }

        delegate: card
    }

    Component.onCompleted: {
        mainModel.append({"day": "Monday", "weathertype": "Pui Pui", "temp": "Makhan", "windspeed" : "18 km/h", "precipitation": "23%"});
        mainModel.append({"day": "Tuesday", "weathertype": "ThunderStorm", "temp": "15 C", "windspeed" : "25 km/h", "precipitation": "10%"});
        mainModel.append({"day": "Wednesday", "weathertype": "Summer", "temp": "32 C", "windspeed" : "32 km/h", "precipitation": "56%"});
        mainModel.append({"day": "Thursday", "weathertype": "Haze", "temp": "28 C", "windspeed" : "23 km/h", "precipitation": "28%"});
        mainModel.append({"day": "Friday", "weathertype": "Hot", "temp": "48 C", "windspeed" : "34 km/h", "precipitation": "32%"});
        mainModel.append({"day": "Saturday", "weathertype": "Hot", "temp": "57 C", "windspeed" : "21 km/h", "precipitation": "24%"});
    }

    Component {
        id: card
            
        Kirigami.Card {

            height: view.cellHeight - Kirigami.Units.largeSpacing
                        
            Text{
                text: model.day
                padding: 5
            }
            contentItem: Row {
                id: content

                spacing: Kirigami.Units.largeSpacing

                Image{
                    width: 130; height: 100
                    source: "qrc:/rainy.png"
                }

                Controls.Label {
                    wrapMode: Text.WordWrap
                    text: "Weather " + model.temp
                    padding: 50
                }

                Column{

                    spacing: Kirigami.Units.largeSpacing

                    Controls.Label {
                        wrapMode: Text.WordWrap
                        text: "windspeed: " + model.windspeed
                    }

                    Controls.Label {
                        wrapMode: Text.WordWrap
                        text: "Temp: " + model.windspeed
                    }

                    Controls.Label {
                        wrapMode: Text.WordWrap
                        text: "Wind: " + model.windspeed
                    }
                }
            }   
        }
    }
}