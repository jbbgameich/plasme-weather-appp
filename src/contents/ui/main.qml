import QtQuick 2.6
import org.kde.kirigami 2.4 as Kirigami
import QtQuick.Controls 2.5 as Controls
import QtQuick.Layouts 1.11

Kirigami.ApplicationWindow {
    id: root

    title: "Weather"

    pageStack.initialPage: mainPageComponent

    StackLayout {
        width: parent.width
        height: parent.height
        currentIndex: bar.currentIndex
        
       WeatherCards{
           id: pui
       }

        Kirigami.ScrollablePage  {
            id: bangTab
            Text{
                text: "banglore"
            }
        }

        Kirigami.ScrollablePage  {
            id: japTab
            Text{
                text: "banglore"
            }
        }
        
    }

    header: Controls.TabBar {
        id: bar
        width: parent.width
        Controls.TabButton {
            text: qsTr("Kolkata")
        }
        Controls.TabButton {
            text: qsTr("Banglore")
        }
        Controls.TabButton {
            text: qsTr("Japan")
        }
    }
}
